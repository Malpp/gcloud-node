import Config from "../config/Config"

import Database from "../database/Database";
import TaskRepository from "../task/TaskRepository"

import TaskController from "../task/TaskController"

import Server from "../server/Server"

class App {

    constructor() {
        //Database
        let database = new Database({
            directory: Config.appDirectory,
            name: Config.databaseFileName
        });

        //Repositories
        let taskRepository = new TaskRepository(database);

        //Controllers
        let taskController = new TaskController(taskRepository);

        //Server
        this.server = new Server(taskController);
    }

    start() {
        this.server.listen();
    }

}

export default App;