import {expect} from "chai";

import TaskRepository from "./TaskRepository"
import Database from "../database/Database";

let database = null;
let taskRepository = null;

beforeEach(() => {
    database = new Database({name : "TaskRepositoryTest", isMemory: true}); //Memory database for testing purposes
    taskRepository = new TaskRepository(database);
});

afterEach(() => {
    if (database) database.close(true); //Force close the memory database clears it
    database = null;
    taskRepository = null;
});

it("set the task id when added", () => {
    let task = {text: "Sample1"};

    taskRepository.insert(task);

    expect(task.id).to.exist;
});

describe("can find", () => {

    it("all tasks", () => {
        let tasks = [{text: "Sample1"}, {text: "Sample2"}];
        tasks.map(it => taskRepository.insert(it));

        expect(taskRepository.findAll()).to.deep.equal(tasks);
    });

    it("a task by id", () => {
        let task = {text: "Sample1"};
        let tasks = [task, {text: "Sample2"}];
        tasks.map(it => taskRepository.insert(it));

        expect(taskRepository.findById(task.id)).to.deep.equal(task);
    });

});

it("can add a new task", () => {
    let task = {text: "New Sample"};
    let tasks = [{text: "Sample1"}, {text: "Sample2"}];
    tasks.map(it => taskRepository.insert(it));

    taskRepository.insert(task);

    expect(taskRepository.findById(task.id)).to.deep.equal(task);
});

it("can delete an existing task", () => {
    let task = {text: "Sample1"};
    let tasks = [task, {text: "Sample2"}];
    tasks.map(it => taskRepository.insert(it));

    taskRepository.delete(task.id);

    expect(taskRepository.findById(task.id)).to.be.null;
});