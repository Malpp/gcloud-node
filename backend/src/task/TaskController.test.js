import chai, {expect} from "chai";
import chaiHttp from "chai-http";

import Database from "../database/Database"
import TaskRepository from "./TaskRepository"
import TaskController from "./TaskController"
import Server from "../server/Server"

chai.use(chaiHttp);
let request = chai.request;

let database = null;
let taskRepository = null;
let taskController = null;
let server = null;

beforeEach(() => {
    database = new Database({name : "TaskControllerTest", isMemory: true}); //Memory database for testing purposes
    taskRepository = new TaskRepository(database);
    taskController = new TaskController(taskRepository);
    server = new Server(taskController);
});

afterEach(() => {
    if (database) database.close(true); //Force close the memory database clears it
    database = null;
    taskRepository = null;
    taskController = null;
    server = null;
});

describe("can retrieve", () => {

    it("all tasks", (done) => {
        let tasks = [{text: "Sample1"}, {text: "Sample2"}];
        tasks.map(it => taskRepository.insert(it));

        request(server)
            .get("/api/task")
            .end(function (error, response) {
                expect(error).to.be.null;
                expect(response).to.have.status(200);
                expect(response.body).to.deep.include.members(tasks);
                done(); //Test done
            });
    });

    it("a task by id", (done) => {
        let task = {text: "Sample1"};
        let tasks = [task, {text: "Sample2"}];
        tasks.map(it => taskRepository.insert(it));

        request(server)
            .get("/api/task/" + task.id)
            .end(function (error, response) {
                expect(error).to.be.null;
                expect(response).to.have.status(200);
                expect(response.body).to.deep.equal(task);
                done(); //Test done
            });
    });

});

it("can add a new task", (done) => {
    let task = {text: "New Sample"};
    let tasks = [{text: "Sample1"}, {text: "Sample2"}];
    tasks.map(it => taskRepository.insert(it));

    request(server)
        .post("/api/task")
        .send(task)
        .end(function (error, response) {
            expect(error).to.be.null;
            expect(response).to.have.status(200);
            expect(response.body).to.deep.include(task);
            expect(taskRepository.findById(response.body.id)).to.exist;
            done(); //Test done
        });
});

it("can delete an existing task", (done) => {
    let task = {text: "Sample1"};
    let tasks = [task, {text: "Sample2"}];
    tasks.map(it => taskRepository.insert(it));

    request(server)
        .delete("/api/task/" + task.id)
        .end(function (error, response) {
            expect(error).to.be.null;
            expect(response).to.have.status(200);
            expect(taskRepository.findById(task.id)).to.not.exist;
            done(); //Test done
        });
});