import Controller from "../controller/Controller"

class TaskController extends Controller {

    constructor(taskRepository) {
        super("/api/task");

        this.taskRepository = taskRepository;

        this.get("/", this.findAllTasks);
        this.get("/:id", this.findTaskById);
        this.post("/", this.insertTask);
        this.delete("/:id", this.deleteTask);
    }

    findAllTasks(request, response) {
        let tasks = this.taskRepository.findAll();

        response.json(tasks);
    }

    findTaskById(request, response) {
        let task = this.taskRepository.findById(request.params.id);

        if (task) {
            response.json(task);
        } else {
            response.status(404);
            response.end();
        }
    }

    insertTask(request, response) {
        let task = request.body;
        this.taskRepository.insert(task);

        response.json(task);
    }

    deleteTask(request, response) {
        this.taskRepository.delete(request.params.id);

        response.end();
    }

}

export default TaskController;