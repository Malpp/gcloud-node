class TaskRepository {

    constructor(database) {
        this.database = database;
    }

    findAll() {
        let rows = this.database.executeQuery("SELECT id, text FROM task");
        return rows.map(it => ({
            id: it.id,
            text: it.text
        }));
    }

    findById(id) {
        let rows = this.database.executeQuery("SELECT id, text FROM task WHERE id = ?", [
            id
        ]);
        if (rows.length > 0) {
            let row = rows[0];
            return {
                id: row.id,
                text: row.text
            };
        } else {
            return null;
        }
    }

    insert(task) {
        this.database.startTransaction();
        task.id = this.database.executeNonQuery(
            "INSERT INTO task (text) VALUES (?)",
            [task.text]
        );
        this.database.endTransaction();
    }

    delete(id) {
        this.database.startTransaction();
        this.database.executeNonQuery(
            "DELETE FROM task WHERE id = ?",
            [id]
        );
        this.database.endTransaction();
    }

}

export default TaskRepository;