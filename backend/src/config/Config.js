import Path from "path"

let Config = {};

Config.isProduction = process.env.NODE_ENV === "production";
Config.appDirectory = Path.join(process.cwd(), ".app");
Config.databaseFileName = "Database.db";
Config.databaseSchemaVersion = 1;
Config.serverPort = 8080;
Config.staticDirectory = Path.join(process.cwd(), "frontend");
Config.staticIndexFile = Path.join(Config.staticDirectory, "index.html");

export default Config;
