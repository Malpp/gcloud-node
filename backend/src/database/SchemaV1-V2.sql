PRAGMA foreign_keys=OFF;

BEGIN;

CREATE TABLE task_new (
  id             INTEGER             NOT NULL        PRIMARY KEY     AUTOINCREMENT,
  text           VARCHAR(1024)       NOT NULL,
  isCompleted    INTEGER             NOT NULL        DEFAULT 0
);

INSERT INTO
  task_new (
    id,
    text
  )
SELECT
  id,
  text
FROM
  task;

DROP TABLE
  task;

ALTER TABLE
  task_new
RENAME TO
  task;

END;

PRAGMA foreign_keys=ON;