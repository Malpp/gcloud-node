CREATE TABLE task (
  id         INTEGER             NOT NULL        PRIMARY KEY     AUTOINCREMENT,
  text       VARCHAR(1024)       NOT NULL
);