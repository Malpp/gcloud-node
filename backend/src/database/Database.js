import File from "fs"
import Path from "path"
import SqLite3 from "better-sqlite3"

import Config from "../config/Config"

/*
 * ======================================= PLEASE READ! =======================================
 *
 * Database automatically handles schema upgrades, but you need to follow theses rules.
 *
 * 1. All schema files must be placed in the same folder as this file.
 *
 * 2. All schema files (i.e .sql files with "CREATE TABLE" statements) must follow this naming convention :
 *
 *      SchemaV[0].sql         where [0] is an integral number representing the version number.
 *
 *
 * 3. All schema upgrade files (i.e .sql files with "ALTER TABLE" statements) must follow this naming convention :
 *
 *      SchemaV[0]-V[1].sql    where [0] is an integral number representing the source version number.
 *                                   [1] is an integral number representing the target version number.
 *
 * 4. There must be a schema file for each version, starting at 1. For example :
 *
 *     - SchemaV1.sql
 *     - SchemaV2.sql
 *     - SchemaV3.sql
 *
 *     Not
 *
 *     - SchemaV0.sql
 *     - SchemaV3.sql
 *     - SchemaV4.sql
 *
 * 6. There must be a schema upgrade file between each version. For example :
 *
 *     - SchemaV1.sql
 *     - SchemaV1-V2.sql
 *     - SchemaV2.sql
 *     - SchemaV2-V3.sql
 *     - SchemaV3.sql
 *
 *     Not
 *
 *     - SchemaV1.sql
 *     - SchemaV2.sql
 *     - SchemaV2-V3.sql
 *     - SchemaV3.sql
 *
 * 7. To update to a new version, set the "DATABASE_SCHEMA_VERSION" constant (see Config.js) to
 *    the version number you are targeting.
 *
 * =================================================================================
 * ==> Not following these rules is undocumented behavior! You have been warned. <==
 * =================================================================================
 *
 */

const VERSION_PARAM = "user_version";
const READ_SCHEMA_OPTIONS = {encoding: "utf-8"};

class Database {

    constructor(options) {
        if (!options) {
            throw new Error("Options must be specified.");
        }

        this.isMemory = !!options.isMemory;

        if (!options.directory && !this.isMemory) {
            throw new Error("Directory must be specified in options when not using an an \"On Memory\" database.");
        }
        if (!options.name) {
            throw new Error("Name must be specified in options.");
        }

        this.path = this.isMemory ? options.name : Path.join(options.directory, options.name);
        this.schema = options.schema;
        this.schemaVersion = options.schemaVersion ? options.schemaVersion : (this.schema ? 1 : Config.databaseSchemaVersion);
        this.schemaDirectory = options.schemaDirectory ? options.schemaDirectory : __dirname;

        this.sqlite = null;
        this.isInTransaction = false;

        if (!this.isMemory && this.schema) {
            throw new Error("You can only specify a schema when using an \"On Memory\" database.");
        }

        if (!this.isMemory) {
            if (!File.existsSync(options.directory)) File.mkdirSync(options.directory);
        }

        this.isInitializing = false;
        this.initialize();
    }

    initialize() {
        if (!this.isInitializing) { //Prevent a "Stack Overflow", because the "executeRaw" method calls open that call "initialize".
            this.isInitializing = true;

            let currentVersion = this.version;
            if (currentVersion < this.schemaVersion) {
                while (currentVersion < this.schemaVersion) {
                    if (this.schema) {
                        this.executeRaw(this.schema);

                        currentVersion = this.schemaVersion;
                    }
                    else if (currentVersion === 0) {
                        let schemaPath = Path.join(this.schemaDirectory, "SchemaV" + this.schemaVersion + ".sql");
                        let schemaInstall = File.readFileSync(schemaPath, READ_SCHEMA_OPTIONS);
                        this.executeRaw(schemaInstall);

                        currentVersion = this.schemaVersion;
                    } else {
                        let schemaPath = Path.join(this.schemaDirectory, "SchemaV" + currentVersion + "-V" + (currentVersion + 1) + ".sql");
                        let schemaUpdate = File.readFileSync(schemaPath, READ_SCHEMA_OPTIONS);
                        this.executeRaw(schemaUpdate);

                        currentVersion++;
                    }
                }
                this.version = currentVersion;
            }

            this.isInitializing = false;
        }
    }

    get version() {
        return this.executeQueryParam(VERSION_PARAM);
    }

    set version(value) {
        this.executeNonQueryParam(VERSION_PARAM, value);
    }

    open() {
        if (!this.isOpen) {
            this.sqlite = new SqLite3(this.path, {memory: this.isMemory});
            if (this.isMemory) this.initialize();
        }
    }

    close(force) {
        if (this.isOpen) {
            if (force || !this.isMemory) {
                this.sqlite.close();

                this.sqlite = null;
                this.isInTransaction = false;
            }
            else {
                this.cancelTransaction();
            }
        }
    }

    get isOpen() {
        return this.sqlite !== null;
    }

    startTransaction() {
        if (!this.isInTransaction) {
            this.executeRaw("BEGIN IMMEDIATE TRANSACTION");
            this.isInTransaction = true;
        }
    }

    cancelTransaction() {
        if (this.isInTransaction) {
            this.executeRaw("ROLLBACK TRANSACTION");
            this.isInTransaction = false;
        }
    }

    endTransaction() {
        if (this.isInTransaction) {
            this.executeRaw("END TRANSACTION");
            this.isInTransaction = false;
        }
    }

    executeRaw(statements) {
        let wasOpen = this.isOpen;
        if (!wasOpen) this.open();
        this.sqlite.exec(statements);
        if (!wasOpen) this.close();
    }

    executeNonQuery(statement, params) {
        params = params ? params : [];

        let wasOpen = this.isOpen;
        if (!wasOpen) this.open();
        // noinspection JSUnresolvedFunction
        let preparedStatement = this.sqlite.prepare(statement);
        // noinspection JSUnresolvedVariable
        let id = preparedStatement.run(params).lastInsertROWID;
        if (!wasOpen) this.close();
        return id;
    }

    executeNonQueryParam(name, value) {
        let wasOpen = this.isOpen;
        if (!wasOpen) this.open();
        this.sqlite.pragma(name + " = " + value);
        if (!wasOpen) this.close();
    }

    executeQuery(statement, params) {
        params = params ? params : [];

        let wasOpen = this.isOpen;
        if (!wasOpen) this.open();
        // noinspection JSUnresolvedFunction
        let preparedStatement = this.sqlite.prepare(statement);
        let rows = preparedStatement.all(params);
        if (!wasOpen) this.close();
        return rows;
    }

    executeQueryParam(name) {
        let wasOpen = this.isOpen;
        if (!wasOpen) this.open();
        let value = this.sqlite.pragma(name, true);
        if (!wasOpen) this.close();
        return value;
    }

}

export default Database;
