import {expect} from "chai";
import Database from "./Database"

/*
 * ======================================= PLEASE READ! =======================================
 *
 * Database tests are done in memory only, not on file system.
 *
 * Jest runs tests in parallel. To prevent problems, each database used during a test must have a unique name.
 */

let database = null;
afterEach(() => {
    if (database) database.close(true); //Force close the memory database clears it
    database = null;
});

const NAME = "DatabaseTest";
const SCHEMA =
    "CREATE TABLE sample (\n" +
    "  id         INTEGER             NOT NULL        PRIMARY KEY,\n" +
    "  text       VARCHAR(1024)       NOT NULL\n" +
    ");";

const QUERY = "SELECT * from sample";
const NON_QUERY = "INSERT INTO sample VALUES (?,?)";
const NON_QUERY_PARAMS = [1, "Sample"];

describe("database cannot be created", () => {

    it("if directory is not specified when not in memory", () => {
        expect(() => new Database({name: "Sample.db"})).to.throw();
    });

    it("if name is not specified", () => {
        expect(() => new Database({directory: ".sample"})).to.throw();
    });

    it("if schema is specified when not in memory", () => {
        expect(() => new Database({directory: ".sample", name: "Sample.db", schema: "CREATE SAMPLE;"})).to.throw();
    });

});

describe("the database version", () => {

    it("is 1 by default when in memory", () => {
        database = new Database({name : NAME, schema: SCHEMA, isMemory: true});

        expect(database.version).to.equal(1);
    });

    it("is the specified one", () => {
        database = new Database({name : NAME, schema: SCHEMA, schemaVersion: 42, isMemory: true});

        expect(database.version).to.equal(42);
    });

});

it("can execute basic query and non-query", () => {
    database = new Database({name : NAME, schema: SCHEMA, isMemory: true});

    database.executeNonQuery(NON_QUERY, NON_QUERY_PARAMS);
    let data = database.executeQuery(QUERY);

    expect(data).to.be.not.empty;
    expect(data[0].id).to.equal(NON_QUERY_PARAMS[0]);
    expect(data[0].text).to.equal(NON_QUERY_PARAMS[1]);
});

describe("transations", () => {

    it("can be commit", () => {
        database = new Database({name : NAME, schema: SCHEMA, isMemory: true});

        database.startTransaction();
        database.executeNonQuery(NON_QUERY, NON_QUERY_PARAMS);
        database.endTransaction();
        let data = database.executeQuery(QUERY);

        expect(data).to.be.not.empty;
    });

    it("can be cancelled", () => {
        database = new Database({name : NAME, schema: SCHEMA, isMemory: true});

        database.startTransaction();
        database.executeNonQuery(NON_QUERY, NON_QUERY_PARAMS);
        database.cancelTransaction();
        let data = database.executeQuery(QUERY);

        expect(data).to.be.empty;
    });

});




