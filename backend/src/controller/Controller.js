import {Router} from "express"

class Controller {

    constructor(route) {
        this.router = Router({});
        this.route = route;
    }

    get(route, handler) {
        this.router.get(route, handler.bind(this));
    }

    post(route, handler) {
        this.router.post(route, handler.bind(this));
    }

    delete(route, handler) {
        this.router.delete(route, handler.bind(this));
    }

}

export default Controller;