import chai, {expect} from "chai";
import {fake, stub} from "sinon";
import sinonChai from "sinon-chai"

import Controller from "./Controller"

chai.use(sinonChai);

describe("can bind", () => {

    [
        "get",
        "post",
        "delete"
    ].map(method => {
        it("to a HTTP " + method.toUpperCase() + " method", () => {
            let controller = new Controller("/sample");
            stub(controller.router, method);

            let callback = fake();
            let bindCallback = fake();
            let bind = stub(callback, "bind");
            bind.returns(bindCallback);
            controller[method]("/test", callback);

            expect(controller.router[method]).to.have.been.calledWith("/test", bindCallback);
        });

    });

});