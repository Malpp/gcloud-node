import Express from "express"
import BodyParser from "body-parser"

import Config from "../config/Config"

class Server {

    constructor(...controllers) {
        this.server = Express();
        this.server.use(BodyParser.json());

        controllers.map(it => {
            if (it.route) this.server.use(it.route, it.router);
            else this.server.use(it.router);
        });

        if (Config.isProduction) {
            this.server.use(Express.static(Config.staticDirectory));
            this.server.get("/*", (_, response) => response.sendFile(Config.staticIndexFile));
        }
    }

    //Used by testing framework. Name must stay the same.
    // noinspection JSUnusedGlobalSymbols
    address() {
        return this.currentServer ? this.currentServer.address() : null;
    }

    //Used by testing framework. Name must stay the same.
    listen(port) {
        this.currentServer = this.server.listen(port ? port : Config.serverPort);
        return this;
    }

    //Used by testing framework. Name must stay the same.
    // noinspection JSUnusedGlobalSymbols
    close() {
        if (this.currentServer) this.currentServer.close();
        this.currentServer = null;
    }

}

export default Server;