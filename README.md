# Google Cloud - Sample Node JS

## Structure du projet

Le projet est divisé en deux modules : ```frontend``` et ```backend```. Les noms de ces modules 
devraient être assez explicite pour comprendre ce qu'ils contienenent.

Lors du développement, vous aurez besoin de démarrer les deux projets en même temps. Vous pouvez soit le faire avec
la commande ```yarn start``` dans chacun des modules ou utiliser votre IDE. Vous pouvez aussi exécuter les tests avec 
la commande ```yarn test``` dans chacun des modules. Les tests unitaires peuvent aussi être exécutés à partir de votre 
IDE.

## Mise en route

### Dépendances

Vous aurez besoin de :

 * NodeJS > v8.10.0
 * Yarn > 1.7.0
 
Suivez les instructions d'installation en fonction de votre système d'exploitation.

### Installation des modules node

Avant de pouvoir exécuter quoi que ce soit, faites un ```yarn install``` dans les dossiers ```frontend``` et 
 ```backend```. Cela peut prendre un moment (2 à 5 minutes selon votre poste).

### IntelliJ IDEA (ou WebStorm)

#### Préparation

Installez le pluging ```NodeJS```. Cela vous permettra de déboguer le module ```backend```. Redémarrez
votre IDE une fois le plugin installé.

#### Démarer un module

Pour démarrer l'un des deux modules (```frontend``` ou ```backend```), créez une configuration de type
```npm``` et utilisez les paramètres suivants (en prenant soin de les adapter selon le module) :

![Configuration](.doc/backend_conf.png)

#### Déboguer le module ```backend```

Le module ```backend``` est déjà prêt à être débogué directement dans Intellij IDEA. Vous n'avez qu'à 
démarrer le module en mode ```Debug```.

Notez que cela est possible grâce à la variable d'environnement ```$NODE_DEBUG_OPTION```. Lorsque 
Intellij démarre une configuration ```npm ``` en mode ```Debug```, il envoie les paramètres 
nécessaires à ```NodeJs``` via cette variable d'environnement.

```json
{
  "scripts": {
    "start": "nodemon $NODE_DEBUG_OPTION src/index.js --watch src --exec babel-node"
  }
}
```

#### Déboguer le module ```frontend```

Il est fortement recommandé de déboguer ce module directement dans votre navigateur. Utilisez les 
outils de développement du navigateur (```CTRL-SHIFT-I```) pour y accéder.

![Déboguage dans le navigateur](.doc/browser_debug.png)

Pour une meilleure expérience, il est recommandé d'installer le plugin ```react``` dans votre 
navigateur. Consultez la section [Navigateur](README.MD#Navigateur) pour les détails.

#### Démarrer les tests unitaires

Les modules ```frontend``` et ```backend``` contiennent déjà tout ce dont vous avez besoin pour
écrire et exécuter des tests. Ils utilisent [Jest](http://jestjs.io/docs/en/using-matchers) et 
[Mocha](https://mochajs.org/#getting-started) comme ```test runner``` respectivement.

**IMPORTANT!!!** Le module ```frontend``` fut créé avec 
[create-react-app](https://github.com/facebook/create-react-app). Or, ```create-react-app``` ne 
[supporte que Jest 20.0.4](https://github.com/facebook/create-react-app/issues/4622). Vous devrez 
donc faire attention à utiliser l'api disponible pour les versions < 22 (disponible 
[ici](http://jestjs.io/docs/en/22.0/api)). 

Les deux modules utilisent [Chai](http://www.chaijs.com/api/bdd/) pour les assetions et 
[Sinon](http://sinonjs.org/releases/v6.0.1/stubs/) pour les Mocks.

**IMPORTANT** Le module ```frontend``` utilise parfois les [manual mocks](http://jestjs.io/docs/en/22.0/manual-mocks) à
la place de ```sinon```. En guise d'exemple, consultez ```TaskRepository.js``` et son mock dans le dossier 
```___mocks___```. Le module ```frontend``` utilise aussi [Enzyme](http://airbnb.io/enzyme/#shallow-rendering) pour 
faciliter les tests de composants ```React```.

Pour démarrer les tests d'un des des deux modules (```frontend``` ou ```backend```), créez des configurations de type 
```Jest``` et ```Mocha``` (respectivement) avec les paramètres suivants :

![Configuration](.doc/frontend_tests.png)

![Configuration](.doc/backend_tests.png)

## Navigateur

Installez le plugin ```react``` dans votre navigateur. Cela vous permettra de consulter les ```props``` et le ```state``` 
de vos ```components```.

![Plugin React](.doc/react_plugin.png)

## Résolution de problèmes

### Cannot find module './Something.css' from 'Something.js'

Vous avez tenté d'exécuter les tests à partir de l'IDE sans avoir préalablement compilé
le projet. Exécutez ```yarn test``` dans les dossiers ```backend``` et ```frontend``` et
réessayez.